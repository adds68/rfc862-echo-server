#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>

#include <netinet/in.h>
#include <unistd.h>

int main() {

  int SERVER_SOCKET = 9002;

  char server_message[256] = "Hello from the server!";

  // create a new socket
  int server_socket;
  server_socket = socket(AF_INET, SOCK_STREAM, 0);

  // define the sever address
  struct sockaddr_in server_address;
  server_address.sin_family = AF_INET;
  server_address.sin_port = htons(SERVER_SOCKET);
  server_address.sin_addr.s_addr = INADDR_ANY;

  // bind the socket to network address
  bind(server_socket, (struct sockaddr*) &server_address, sizeof(server_address));

  listen(server_socket, 5);

  int client_socket;
  client_socket = accept(server_socket, NULL, NULL);

  // send message to client
  send(client_socket, server_message, sizeof(server_message), 0);

  close(server_socket);

  return 0;

}
